import React from 'react';
import {StyleSheet, View, Button, Text} from 'react-native';
import {connect} from 'react-redux';
import {changeCount} from './src/redux/action/counts';
import {bindActionCreators} from 'redux';
import WrapperContainer from './src/views/wrapper/container/WrapperContainer';
const App = (props) => {
  return <WrapperContainer />;
};
const mapStateToProps = (state) => {
  console.log('state', state);
  return {
    count: state.countReducer.count,
    user: state.userReducer.user,
  };
};
const ActionCreators = {changeCount};
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
