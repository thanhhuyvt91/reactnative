import {Text, View} from 'react-native';
import React from 'react';
import {StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {TextInput, Button} from 'react-native-paper';

const SignUp = (props) => {
  return (
    <View style={{marginLeft: '10%', marginRight: '10%'}}>
      <View>
        <Text style={styles.titleText}>Sign Up</Text>
      </View>
      <View style={{alignItems: 'center', paddingTop: 14}}>
        <TextInput
          style={{width: '100%', paddingBottom: 16}}
          mode="outlined"
          label="UserName"
          left={
            <TextInput.Icon
              name={() => <Icon name="users" size={24} color="#adb5bd" />}
            />
          }
        />
        <TextInput
          style={{width: '100%', paddingBottom: 16}}
          mode="outlined"
          label="Password"
          left={
            <TextInput.Icon
              name={() => <Icon name="shield" size={24} color="#adb5bd" />}
            />
          }
        />
        <TextInput
          style={{width: '100%', paddingBottom: 16}}
          mode="outlined"
          label="Confirm Password"
          left={
            <TextInput.Icon
              name={() => <Icon name="shield" size={24} color="#adb5bd" />}
            />
          }
        />
        <TextInput
          style={{width: '100%', paddingBottom: 32}}
          mode="outlined"
          label="Email"
          left={
            <TextInput.Icon
              name={() => <Icon name="envelope" size={24} color="#adb5bd" />}
            />
          }
        />
        <Button
          style={{
            width: 250,
            backgroundColor: 'steelblue',
            height: 50,
            marginLeft: 0,
            marginRight: 0,
            justifyContent: 'center',
          }}
          mode="contained">
          SIGN IN
        </Button>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  titleText: {
    color: '#2f752f',
    fontSize: 36,
    textAlign: 'center',
    paddingTop: '30%',
  },
});

export default SignUp;
