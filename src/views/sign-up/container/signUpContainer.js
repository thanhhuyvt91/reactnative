import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import SignUp from '../component/signUp';
const SignUpContainer = (props) => {
  let {actions, count, user} = props;
  return <SignUp />;
};
const mapStateToProps = (state) => {
  console.log('state', state);
  return {
    count: state.countReducer.count,
    user: state.userReducer.user,
  };
};
const ActionCreators = {};
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpContainer);
