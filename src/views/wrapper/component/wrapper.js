import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import SignUpContainer from '../../sign-up/container/signUpContainer';
import {createStackNavigator} from '@react-navigation/stack';
import WrapContainer from '../../wrap-container/container/WrapContainer';
import signInContainer from '../../sign-in/container/signInContainer';
const Stack = createStackNavigator();
const Wrapper = (props) => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="SignIn">
        <Stack.Screen
          name="SignIn"
          component={signInContainer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignUp"
          options={{headerShown: false}}
          component={SignUpContainer}
        />
        <Stack.Screen
          name="Wrap"
          options={{headerShown: false}}
          component={WrapContainer}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default Wrapper;
