import Wrap from '../component/wrap';
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
const WrapContainer = (props) => {
  let {actions, count, user} = props;
  return <Wrap />;
};
const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps)(WrapContainer);
