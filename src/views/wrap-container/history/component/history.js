import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Card, Title, Paragraph, IconButton} from 'react-native-paper';
import {Image} from 'react-native';
const Tab = createBottomTabNavigator();
const History = (props) => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>My History</Text>
      </View>
      <View style={{flexDirection: 'row-reverse'}}>
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
        <View style={{width: 16, height: 1}} />
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
      </View>
      <View style={{flexDirection: 'row-reverse', paddingTop: 16}}>
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
        <View style={{width: 16, height: 1}} />
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
      </View>
      <View style={{flexDirection: 'row-reverse', paddingTop: 16}}>
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
        <View style={{width: 16, height: 1}} />
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
      </View>
      <View style={{flexDirection: 'row-reverse', paddingTop: 16}}>
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
        <View style={{width: 16, height: 1}} />
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
      </View>
      <View style={{flexDirection: 'row-reverse', paddingTop: 16}}>
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
        <View style={{width: 16, height: 1}} />
        <View style={{flex: 0.5}}>
          <Card>
            <Card.Title
              title="Laptop - PH"
              titleStyle={{fontSize: 14}}
              subtitleStyle={{fontSize: 12}}
              subtitle="10/10/2012"
              left={(props) => (
                <Image
                  source={{uri: 'https://picsum.photos/200/300'}}
                  style={{width: 40, height: 40, borderRadius: 5}}
                />
              )}
            />
          </Card>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
  },
  title: {
    borderBottomWidth: 1,
    borderBottomColor: '#0000001f',
    margin: '5%',
  },
});

export default History;
