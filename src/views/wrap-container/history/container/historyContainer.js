import History from '../component/history';
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
const HistoryContainer = (props) => {
  return <History />;
};
const mapStateToProps = (state) => {
  return {};
};
const ActionCreators = {};
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryContainer);
