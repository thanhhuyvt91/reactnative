import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {SafeAreaView} from 'react-native';
import {StyleSheet} from 'react-native';
import Registraion from '../component/registration';
const RegistraionContainer = (props) => {
  let {actions, count, user} = props;
  return (
    <SafeAreaView style={styles.container}>
      <Registraion />
    </SafeAreaView>
  );
};
const mapStateToProps = (state) => {
  console.log('state', state);
  return {
    count: state.countReducer.count,
    user: state.userReducer.user,
  };
};
const ActionCreators = {};
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    marginTop: 16,
  },
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegistraionContainer);
