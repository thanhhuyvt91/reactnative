import {ActivityIndicator, View} from 'react-native';
import React, {useState} from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import {Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text} from 'react-native-elements';
import DatePicker from 'react-native-date-picker';
import {Image} from 'react-native-elements';
import {
  launchCamera,
  launchImageLibrary,
  ImagePicker,
} from 'react-native-image-picker';
const imgDefault = '/red.png';
const Evidence = (props) => {
  const [imgFront, setImgFront] = useState(imgDefault);
  const [imgSide, setImgSide] = useState(imgDefault);
  const [imgBack, setImgBack] = useState(imgDefault);
  const data = [
    {id: 1, name: 'Product Image Front', uri: 'https://picsum.photos/200/200'},
    {id: 2, name: 'Product Image Side', uri: 'https://picsum.photos/200/200'},
    {id: 3, name: 'Product Image Back', uri: 'https://picsum.photos/200/200'},
  ];
  const handleChoosePhoto = (id) => {
    console.log(id);
    const options = {};
    launchImageLibrary(options, (response) => {
      console.log('response', response);
      switch (id) {
        case 1:
          setImgFront(response.uri);
          break;
        case 2:
          setImgSide(response.uri);
          break;
        case 3:
          setImgBack(response.uri);
          break;

        default:
          break;
      }
      f;
    });
  };
  const handleCamera = (id) => {
    console.log(id);
    const options = {};
    launchCamera(options, (response) => {
      console.log('response', response);
      switch (id) {
        case 1:
          setImgFront(response.uri);
          break;
        case 2:
          setImgSide(response.uri);
          break;
        case 3:
          setImgBack(response.uri);
          break;

        default:
          break;
      }
    });
  };
  return (
    <ScrollView style={styles.scrollView}>
      <View
        style={{
          marginLeft: '10%',
          marginRight: '10%',
          alignItems: 'center',
        }}>
        {data.map((item, index) => (
          <View key={index}>
            <View style={{paddingBottom: 12, flexDirection: 'row'}}>
              <View>
                <Text>
                  {item.name}
                  <View style={{width: 16, height: 1}} />
                  <Icon
                    raised
                    size={24}
                    color="#2089dc"
                    name="folder-open"
                    type="font-awesome"
                    onPress={() => handleChoosePhoto(item.id)}
                  />
                  <View style={{width: 16, height: 1}} />
                  <Icon
                    raised
                    size={24}
                    color="#2089dc"
                    name="camera"
                    type="font-awesome"
                    onPress={() => handleCamera(item.id)}></Icon>
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', paddingBottom: 12}}>
              <Image
                source={{
                  uri: index === 0 ? imgFront : index === 1 ? imgSide : imgBack,
                  // uri: imgDefault,
                }}
                style={{width: 300, height: 200}}
              />
            </View>
          </View>
        ))}
        <View style={{alignItems: 'center', paddingTop: '10%'}}>
          <Button
            title="NEXT  "
            iconRight
            icon={<Icon name="arrow-right" size={20} color="white" />}
            buttonStyle={{
              backgroundColor: 'steelblue',
              width: 200,
            }}
            // onPress={() => props.next(2)}
          />
        </View>
        <View style={{alignItems: 'center', paddingTop: '5%'}}>
          <Button
            title="   BACK"
            icon={<Icon name="arrow-left" size={20} color="white" />}
            buttonStyle={{backgroundColor: '#8e8b8c', width: 200}}
            onPress={() => props.next(1)}
          />
        </View>
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Evidence;
