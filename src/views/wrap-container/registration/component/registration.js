import React, {useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import Swiper from 'react-native-swiper';
import Evidence from './evidence';
import ProductInfo from './productInfo';
import ScanBarCode from './scanBarCode';

const PAGES = ['Page 1', 'Page 2', 'Page 3'];

const firstIndicatorStyles = {
  stepIndicatorSize: 40,
  currentStepIndicatorSize: 40,
  separatorStrokeWidth: 1,
  currentStepStrokeWidth: 2,
  separatorFinishedColor: '#3f51b5',
  separatorUnFinishedColor: '#3f51b5',
  stepIndicatorFinishedColor: '#3f51b5',
  stepIndicatorUnFinishedColor: '#3f51b5',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 15,
  currentStepIndicatorLabelFontSize: 15,
  stepIndicatorLabelCurrentColor: '#000000',
  stepIndicatorLabelFinishedColor: '#ffffff',
  // stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
  labelColor: '#666666',
  labelSize: 12,
  currentStepLabelColor: '#3f51b5',
};
export default function Registraion() {
  const [currentPage, setCurrentPage] = useState(0);

  const onStepPress = (position) => {
    console.log('position', position);
    setCurrentPage(position);
  };

  const renderViewPagerPage = (data) => {
    console.log('data', data);
    if (data === 0) {
      return <ProductInfo next={onStepPress} />;
    } else if (data === 1) {
      return <ScanBarCode next={onStepPress} />;
    } else if (data === 2) {
      return <Evidence next={onStepPress} />;
    }
  };
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>Registration product</Text>
      </View>
      <View style={styles.stepIndicator}>
        <StepIndicator
          customStyles={firstIndicatorStyles}
          currentPosition={currentPage}
          labels={['Production Info', 'Scan BarCode', 'Evidence']}
          stepCount={3}
          onPress={onStepPress}
        />
      </View>
      <Swiper style={{flexGrow: 1}} loop={false} index={currentPage}>
        {renderViewPagerPage(currentPage)}
      </Swiper>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  stepIndicator: {
    paddingBottom: 20,
  },
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  stepLabel: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#999999',
  },
  stepLabelSelected: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#4aae4f',
  },
  title: {
    borderBottomWidth: 1,
    borderBottomColor: '#0000001f',
    margin: '5%',
  },
});
