import {ActivityIndicator, View} from 'react-native';
import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text} from 'react-native-elements';
import {TextInput, Button} from 'react-native-paper';

const ProductInfo = (props) => {
  const [date, setDate] = useState(new Date());
  return (
    <View style={styles.container}>
      <View style={styles.input_control}>
        <View style={{flex: 0.3}}>
          <Text style={styles.label}>Model Name</Text>
        </View>
        <View style={{flex: 0.7}}>
          <TextInput
            mode="outlined"
            theme={{
              roundness: 50,
              colors: {
                primary: 'green',
                underlineColor: 'transparent',
              },
            }}
            style={styles.input}
          />
        </View>
      </View>
      <View style={styles.input_control}>
        <View style={{flex: 0.3}}>
          <Text style={styles.label}>Sell Date</Text>
        </View>
        <View style={{flex: 0.7}}>
          <TextInput
            mode="outlined"
            theme={{
              roundness: 50,
              colors: {
                primary: 'green',
                underlineColor: 'transparent',
              },
            }}
            style={styles.input}
          />
        </View>
      </View>
      <View style={styles.input_control}>
        <View style={{flex: 0.3}}>
          <Text style={styles.label}>Cus Name</Text>
        </View>
        <View style={{flex: 0.7}}>
          <TextInput
            mode="outlined"
            theme={{
              roundness: 50,
              colors: {
                primary: 'green',
                underlineColor: 'transparent',
              },
            }}
            style={styles.input}
          />
        </View>
      </View>
      <View style={styles.input_control}>
        <View style={{flex: 0.3}}>
          <Text style={styles.label}>Cus Phone</Text>
        </View>
        <View style={{flex: 0.7}}>
          <TextInput
            mode="outlined"
            theme={{
              roundness: 50,
              colors: {
                primary: 'green',
                underlineColor: 'transparent',
              },
            }}
            style={styles.input}
          />
        </View>
      </View>
      <View style={{alignItems: 'center', paddingTop: '10%'}}>
        <Button
          onPress={() => props.next(1)}
          color="white"
          labelStyle={styles.f18}
          style={[styles.button]}>
          NEXT
          <View style={{width: 16, height: 1}} />
          <Icon name="arrow-right" size={20} color="white" />
        </Button>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
  },
  label: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingTop: 16,
  },
  input: {
    height: 36,
  },
  button: {
    color: 'white',
    backgroundColor: '#3f51b5',
    width: 180,
    marginLeft: 0,
    marginRight: 0,
    justifyContent: 'center',
    fontSize: 18,
  },
  f18: {
    fontSize: 18,
  },
  input_control: {
    flexDirection: 'row',
    paddingBottom: 24,
  },
});

export default ProductInfo;
