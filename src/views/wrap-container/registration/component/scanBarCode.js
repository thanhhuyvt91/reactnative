import React, {useState} from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text} from 'react-native-elements';
import {launchCamera} from 'react-native-image-picker';
import {Image} from 'react-native-elements';
import {RNCamera} from 'react-native-camera';
const ScanBarCode = (props) => {
  const [camera, setCamera] = useState({
    type: RNCamera.Constants.Type.back,
    flashMode: RNCamera.Constants.FlashMode.auto,
  });
  const [barCode, setBarCode] = useState(null);
  const [isCheck, setIsCheck] = useState(false);
  const [photo, setPhoto] = useState(null);
  const [imageUri, setImageUri] = useState(null);
  const handleCamera = () => {
    setIsCheck(true);
    // const options = {};
    // launchCamera(options, (response) => {
    //   console.log('response', response);
    //   setImageUri(response.uri);
    // });
  };
  const PendingView = () => (
    <View
      style={{
        flex: 1,
        backgroundColor: 'lightgreen',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text>Waiting</Text>
    </View>
  );
  takePicture = async function (camera) {
    const options = {quality: 0.5, base64: true};
    const data = await camera.takePictureAsync(options);
    //  eslint-disable-next-line
    console.log(data.uri);
  };
  barcodeRecognized = ({barcodes}) => {
    barcodes.forEach((barcode) => alert(barcode.data));
  };
  const onBarCodeRead = (e) => {
    if (e.data) {
      alert('Barcode value is' + e.data, 'Barcode type is' + e.type);
      setBarCode(e.data);
      setIsCheck(false);
    }
  };
  return (
    <View
      style={{
        marginLeft: '10%',
        marginRight: '10%',
        alignItems: 'center',
      }}>
      <View>
        <Text>Scan BarCode</Text>
      </View>
      <View>
        <Icon
          raised
          size={40}
          color="#2089dc"
          name="camera"
          type="font-awesome"
          onPress={handleCamera}></Icon>
      </View>
      <View>
        {isCheck && (
          <RNCamera
            style={styles.preview}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.on}
            captureAudio={false}
            googleVisionBarcodeMode={
              RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeMode
                .ALTERNATE
            }
            googleVisionBarcodeType={
              RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.ALL
            }
            // onBarCodeRead={barcodeRecognized}
            onBarCodeRead={onBarCodeRead}
            // onGoogleVisionBarcodesDetected={barcodeRecognized}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}>
            {({camera, status}) => {
              if (status !== 'READY') return <PendingView />;
              return (
                <View
                  style={{
                    flex: 0,
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => this.takePicture(camera)}
                    style={styles.capture}>
                    <Text style={{fontSize: 14}}> SNAP </Text>
                  </TouchableOpacity>
                </View>
              );
            }}
          </RNCamera>
        )}
      </View>
      <View style={{alignItems: 'center', paddingTop: '5%'}}>
        <Image source={{uri: imageUri}} style={{width: 300, height: 200}} />
      </View>
      <View>
        <Text>{barCode}</Text>
      </View>
      <View style={{alignItems: 'center', paddingTop: '10%'}}>
        <Button
          title="NEXT  "
          iconRight
          icon={<Icon name="arrow-right" size={20} color="white" />}
          buttonStyle={{
            backgroundColor: 'steelblue',
            width: 200,
          }}
          onPress={() => props.next(2)}
        />
      </View>
      <View style={{alignItems: 'center', paddingTop: '5%'}}>
        <Button
          title="   BACK"
          icon={<Icon name="arrow-left" size={20} color="white" />}
          buttonStyle={{backgroundColor: '#8e8b8c', width: 200}}
          onPress={() => props.next(0)}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  preview: {
    // flex: 1,
    // alignItems: 'center',
    // width: 300,
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

export default ScanBarCode;
