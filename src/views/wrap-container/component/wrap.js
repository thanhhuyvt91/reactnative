import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HistoryContainer from '../history/container/historyContainer';
import homeContainer from '../home/container/homeContainer';
import profileContainer from '../profile/container/profileContainer';
import RegistraionContainer from '../registration/container/registrationContainer';
const Tab = createBottomTabNavigator();
const Wrap = (props) => {
  return (
    <Tab.Navigator headerMode="none">
      <Tab.Screen
        name="Home"
        options={{
          tabBarLabel: 'Home',
          headerShown: false,
          tabBarIcon: ({color}) => <Icon name="home" color={color} size={26} />,
        }}
        component={homeContainer}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Registraion',
          tabBarIcon: ({color}) => <Icon name="xbox" color={color} size={26} />,
        }}
        name="Registraion"
        component={RegistraionContainer}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'History',
          tabBarIcon: ({color}) => (
            <Icon name="list-alt" color={color} size={26} />
          ),
        }}
        name="History"
        component={HistoryContainer}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color}) => (
            <Icon
              type="font-awesome-5"
              name="grin-hearts"
              color={color}
              size={26}
            />
          ),
        }}
        name="Profile"
        component={profileContainer}
      />
    </Tab.Navigator>
  );
};
export default Wrap;
