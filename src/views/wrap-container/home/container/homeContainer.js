import Home from '../component/home';
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
const HomeContainer = (props) => {
  return <Home />;
};
const mapStateToProps = (state) => {
  return {};
};
const ActionCreators = {};
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
