import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Card, Title, Paragraph} from 'react-native-paper';
const Tab = createBottomTabNavigator();
const Profile = (props) => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>My History</Text>
      </View>
      <View>
        <Card>
          <Card.Content>
            <Title>Card title</Title>
            <Paragraph>Card content</Paragraph>
          </Card.Content>
        </Card>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    borderBottomWidth: 1,
    borderBottomColor: '#0000001f',
    margin: '5%',
  },
});

export default Profile;
