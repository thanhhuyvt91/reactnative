import {Text, View} from 'react-native';
import React from 'react';
import {StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {TextInput, Button} from 'react-native-paper';
const SignIn = (props) => {
  return (
    <View style={{marginLeft: '10%', marginRight: '10%'}}>
      <View style={{alignItems: 'center', paddingTop: '40%', width: '100%'}}>
        <TextInput
          style={{width: '100%', paddingBottom: 16}}
          mode="outlined"
          label="Email"
          theme={{
            roundness: 15,
            colors: {
              primary: 'green',
              underlineColor: 'transparent',
            },
          }}
          left={
            <TextInput.Icon
              name={() => <Icon name="user-circle" size={24} color="#adb5bd" />}
            />
          }
        />
        <TextInput
          style={{width: '100%', paddingBottom: 32}}
          mode="outlined"
          label="Password"
          theme={{
            roundness: 15,
            colors: {
              primary: 'green',
              underlineColor: 'transparent',
            },
          }}
          left={
            <TextInput.Icon
              name={() => <Icon name="lock" size={24} color="#adb5bd" />}
            />
          }
        />
        <Button
          color="white"
          onPress={props.redirectWrap}
          labelStyle={styles.fs22}
          style={[styles.button, styles.w250]}>
          <Text>SIGN IN</Text>
        </Button>
        <Text
          style={{color: 'blue', textAlign: 'right', paddingTop: 12}}
          onPress={props.redirectSignUp}>
          Sign Up
        </Text>
      </View>
      <View style={{alignItems: 'center', paddingTop: '40%'}}>
        <Button labelStyle={styles.fs22} color="white" style={styles.button}>
          <Icon name="facebook-f" size={20} color="white" />
          <View style={{width: 16, height: 1}} />
          <Text>SIGN IN</Text>
        </Button>
        <Text>or</Text>
        <Button color="white" labelStyle={styles.fs22} style={styles.button}>
          <Icon name="google" size={20} color="white" />
          <View style={{width: 16, height: 1}} />
          <Text>SIGN IN</Text>
        </Button>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  alignCenter: {
    textAlign: 'center',
    fontWeight: 'bold',
  },
  button: {
    color: 'white',
    backgroundColor: 'steelblue',
    width: 300,
    height: 50,
    marginLeft: 0,
    marginRight: 0,
    justifyContent: 'center',
  },
  w250: {
    width: 250,
  },
  fs22: {
    fontSize: 22,
  },
});

export default SignIn;
