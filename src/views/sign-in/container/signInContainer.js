import SignIn from '../component/signIn';
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
const SignInContainer = (props) => {
  const redirectSignUp = () => {
    console.log('dfsdf');
    props.navigation.navigate('SignUp');
  };
  const redirectWrap = () => {
    props.navigation.navigate('Wrap');
  };
  return <SignIn redirectSignUp={redirectSignUp} redirectWrap={redirectWrap} />;
};
const mapStateToProps = (state) => {
  return {};
};
const ActionCreators = {};
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignInContainer);
