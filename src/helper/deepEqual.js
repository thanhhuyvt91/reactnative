export const isObject = (val) => {
  return val !== null && typeof val === 'object';
};
const deepEqual = (ob1, ob2) => {
  const key1 = Object.keys(ob1);
  const key2 = Object.keys(ob2);
  if (key1.length !== key2.length) {
    return false;
  }
  for (let key of key1) {
    const val1 = ob1[key];
    const val2 = ob2[key];
    const areObjects = isObject(val1) && isObject(val2);
    if (
      (areObjects && !deepEqual(val1, val2)) ||
      (!areObjects && val1 !== val2)
    ) {
      return false;
    }
  }
  return true;
};
const dt1 = [1, 2, 3];
const dt2 = [1, 2, 3];

console.log(deepEqual(dt1, dt2));
