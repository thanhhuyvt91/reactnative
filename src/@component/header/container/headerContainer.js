import Header from '../component/header';
import React from 'react';
const HeaderContainer = (props) => {
  const name = 'Header123';
  const data1 = [
    {id: 11, title: 'Hoa Huong Duong1'},
    {id: 22, title: 'Hoa Huong Duong2'},
    {id: 33, title: 'Hoa Huong Duong3'},
    {id: 44, title: 'Hoa Huong Duong4'},
    {id: 55, title: 'Hoa Huong Duong5'},
  ];
  return <Header name={name} data={data1} />;
};
export default HeaderContainer;
