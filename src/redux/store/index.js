// import {createStore, combineReducers} from 'redux';
// import countReducer from '../reducer/countReducer';
// const rootReducer = combineReducers(countReducer);
// const configureStore = () => {
//   return createStore(rootReducer);
// };
// export default configureStore;

import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {createLogger} from 'redux-logger';
import rootReducer from '../reducer';

const logger = createLogger({
  // ...options
});

const configureStore = (initialState) => {
  const composeEnhancers = composeWithDevTools(applyMiddleware(thunk, logger));
  const store = createStore(rootReducer, initialState, composeEnhancers);

  if (module.hot) {
    module.hot.accept('../reducer', () => {
      store.replaceReducer(require('../reducer').default);
    });
  }

  return store;
};

export default configureStore;
