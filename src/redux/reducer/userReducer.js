const initialState = {
  user: {name: 'Huy', age: 18},
};
const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'COUNTER_CHANGE':
      return {
        ...state,
        user: action.payload,
      };
    default:
      return state;
  }
};
export default userReducer;
